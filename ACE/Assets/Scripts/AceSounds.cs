﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class AceSounds : MonoBehaviour {

    public AudioClip slashA;
    public AudioClip slashB;
    public AudioClip jumpA;
    public AudioClip land;
    public AudioClip footstep;
    public AudioClip holster;
    public AudioClip dash;
    public AudioClip slip;
    public AudioClip ouch;

    public AudioSource audioSource;

    // Use this for initialization
    void Start () {


}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OneshotSlashA()
    {
        audioSource.PlayOneShot(slashA, 0.7F);
    }
    void OneshotSlashB()
    {
        audioSource.PlayOneShot(slashB, 0.7F);
    }
    void OneshotJump()
    {
        audioSource.PlayOneShot(jumpA, 0.7F);
    }
    void OneshotFootstep()
    {
        audioSource.PlayOneShot(footstep, 0.7F);
    }
    void OneshotHolster()
    {
        audioSource.PlayOneShot(holster, 0.7F);
    }
    void Oneshotdash()
    {
        audioSource.PlayOneShot(dash, 0.7F);
    }
    void Oneshotland()
    {
        audioSource.PlayOneShot(land, 0.7F);
    }
    void Oneshotslip()
    {
        audioSource.PlayOneShot(slip, 0.7F);
    }
    void Oneshotouch()
    {
        audioSource.PlayOneShot(ouch, 0.7F);
    }
}
